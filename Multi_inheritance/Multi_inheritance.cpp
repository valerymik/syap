// Multi_inheritance.cpp : Defines the entry point for the console application.

#include "stdafx.h"
using namespace std;

# pragma region classes
// ��������� �������
class Kitchenware 
{
public:
		int Number;
		void PrintNumber(bool prnt)
		{
			if (prnt) 
				cout << "inventory number: " << this->Number << endl;
		}
		virtual void Print(bool prnt) = 0;
		Kitchenware(){
			cout << "Number = ";
			cin >> Number;
		}
		virtual ~Kitchenware() 
		{

		};
};
// ������
class Stove: public virtual Kitchenware
{
public:
	string Color;
	void Print(bool prnt) override
	{
		Kitchenware::PrintNumber(prnt);
		cout << "Color: " + this->Color << endl;
	}
	Stove() :Kitchenware() 
	{
		cout << "Color = ";
		cin >> Color;
	}
};
// ����������
class Pan: public virtual Kitchenware
{
public:
	double Volume;
	void Print(bool prnt) override
	{
		Kitchenware::PrintNumber(prnt);
		cout << "Volume: " << this->Volume << endl;
	}
	Pan() : Kitchenware()
	{
		cout << "Volume = ";
		cin >> Volume;

	}
};
// ������ ��������������
class ElectricStove: public virtual Stove
{
public:
	int Power;
	void Print(bool prnt) override
	{
		Stove::Print(prnt);
		cout << "Power: " << this->Power << endl;
	}
	ElectricStove() : Kitchenware()
	{
		//double pwr;
		cout << "Power = ";
		cin >> Power;
	}
};
// ������ ��������
class GasStove: public virtual Stove
{
public:
	int Consumption;
	void Print(bool prnt) override
	{
		Stove::Print(prnt);
		cout << "Consumption: " << this->Consumption << endl;
	}
	GasStove() :Kitchenware()
	{

		cout << "Consumption = ";
		cin >> Consumption;
	}

};
// ������������
class Multicooker: public virtual Pan, public virtual ElectricStove
{
public:
	bool HasFastcooker;
	void Print(bool prnt) override
	{
		
		ElectricStove::Print(true);
		Pan::Print(false);
		cout << "He has function fastcooker: (0/1)" << this->HasFastcooker << endl;
	}
	Multicooker() :Kitchenware()
	{
		cout << "Has a fastcooker: (0/1) = ";
		cin >> HasFastcooker;
	}

};
# pragma endregion 

#pragma region tree

//���� ������
struct Node
{
	int Height;
	Node * Left;
	Node * Right;
	Kitchenware * Data;
};
typedef Node * PNode;

// ������� ��� ���� hight
int height(PNode & p)
{
	return p ? p->Height : 0;
}

// ��������� balance factor 
int balance_fact(PNode & p)
{
	return height(p->Right) - height(p->Left);
}

// ��������������� ���������� �������� ���� height ��������� ����
void new_height(PNode & p)
{
	int hr = height(p->Right);
	int hl = height(p->Left);

	p->Height = max(hl, hr) + 1;
}

// ��������
void LL(PNode & p)
{
	PNode left_child = p->Left;
	p->Left = left_child->Right;
	left_child->Right = p;
	new_height(p);
	p = left_child;
	new_height(p);
}

void RR(PNode & p)
{
	PNode right_child = p->Right;
	p->Right = right_child->Left;
	right_child->Left = p;
	new_height(p);
	p = right_child;
	new_height(p);
}

void RL(PNode & p)
{
	LL(p->Right);
	RR(p);
}

void LR(PNode & p)
{
	RR(p->Left);
	LL(p);
}

// ������������
void balance(PNode & p)
{
	new_height(p);
	if (balance_fact(p) == 2)
	if (balance_fact(p->Right) >= 0)
		RR(p);
	else
		RL(p);
	else
	if (balance_fact(p) == -2)
	if (balance_fact(p->Left) <= 0)
		LL(p);
	else
		LR(p);
}

// ������� ����
void insert(PNode & root, Kitchenware * x)
{
	if (root == nullptr)
	{
		root = new Node;
		root->Data = x;
		root->Height = 1;
		root->Left = nullptr;
		root->Right = nullptr;
	}
	else
	if (x->Number < root->Data->Number)
		insert(root->Left, x);
	else if (x->Number > root->Data->Number)
		insert(root->Right, x);
	else
		cout << "Object is already in container" << endl;
	balance(root);
}

// ������� ����������� ����
PNode findmin(PNode p)
{
	if (p != nullptr)
		return p->Left ? findmin(p->Left) : p;
	else
		return nullptr;
}

// �������� ���� � ����������� ������ �� ������ p
PNode removemin(PNode p)
{
	if (p != nullptr)
	{
		if (!(p->Left))
			return p->Right;
		p->Left = removemin(p->Left);
		balance(p);
	}
	return p;
}

// �������� �����
void remove(PNode & p, int x)
{
	if (p != nullptr)
	{
		if (x < p->Data->Number)
			remove(p->Left, x);
		else if (x > p->Data->Number)
			remove(p->Right, x);
		else
		{
			PNode q = p->Left;
			PNode r = p->Right;
			delete p;
			if (r == nullptr)
			{
				p = q;
				return;
			}

			PNode min = findmin(r);
			min->Right = removemin(r);
			min->Left = q;
			balance(min);
			p = min;
			return;
		}
	}
	balance(p);
}
#pragma endregion

#pragma region ������

string TypePrint(Kitchenware* p)
{
	if (dynamic_cast<Multicooker*>(p)) 
		return "Multicooker";
	if (dynamic_cast<GasStove*>(p))
		return "GasStove";
	if (dynamic_cast<ElectricStove*> (p))
		return "ElectricStove";
	if (dynamic_cast<Pan*>(p)) 
		return "Pan";
	if (dynamic_cast<Stove*>(p))
		return "Stove";
}
//������ 
void pre_order_output(PNode & p) 
{
	if (p != nullptr)
	{
		cout << TypePrint(p->Data) << endl;
		p->Data->Print(true);
		cout << endl << endl;
		pre_order_output(p->Left);
		pre_order_output(p->Right);
	}
}
//������������ 
void in_order_output(PNode & p) 
{
	if (p != nullptr)
	{
		in_order_output(p->Left);
		cout << TypePrint(p->Data) << endl;
		p->Data->Print(true);
		cout << endl << endl;
		in_order_output(p->Right);
	}
}
//�������� 
void post_order_output(PNode & p) 
{
	if (p != nullptr)
	{
		post_order_output(p->Left);
		post_order_output(p->Right);
		cout << TypePrint(p->Data) << endl;
		p->Data->Print(true);
		cout << endl << endl;
	}
}
// ������ �����
string spaces(int n)
{
	string s = "";
	for (int i = 1; i <= n; i++)
		s += " ";
	return s;
}

void tree_out(PNode & p, int x)
{
	if (p != nullptr)
	{
		tree_out(p->Left, x + 1);
		cout << spaces(x) << p->Data->Number << endl;
		tree_out(p->Right, x + 1);
	}
}

#pragma endregion

# pragma region input for classes



# pragma endregion

int main()
{
	PNode root = nullptr;
	int INP;
	
	
	while (true)
	{

		cout << "1 - Add item" << endl;
		cout << "2 - Remove item" << endl;
		cout << "3 - Show data" << endl;
		cout << "4 - Show tree" << endl;
		try {
			cin >> INP;
		}
		catch (exception)
		{
			cout << "not a number";
		}
			
		
			switch (INP)
			{
			
			case 1:
			{
				cout << "1 - Stove" << endl;
				cout << "2 - Pan" << endl;
				cout << "3 - Electric stove" << endl;
				cout << "4 - Gas stove" << endl;
				cout << "5 - Multicooker" << endl;

				int typeInput;
				cin >> typeInput;

				Kitchenware* k;
				switch (typeInput)
				{
				default:
					k = new Stove;
					break;
				case 2:
					k = new Pan;
					break;
				case 3:
					k = new ElectricStove;
					break;
				case 4:
					k = new GasStove;
					break;
				case 5:
					k = new Multicooker;
					break;
				}
				insert(root, k);
			}
			break;

			case 2:
			{
				cout << "Number: " << endl;
				int number;
				cin >> number;
				remove(root, number);
			}
			break;

			case 3:
			{
				cout << "1 - Pre Order (data-left-right)" << endl;
				cout << "2 - In Order (left-data-right)" << endl;
				cout << "3 - Post Order (left-right-data)" << endl;
				int tr_input;
				cin >> tr_input;

				switch (tr_input)
				{
				case 1:
					pre_order_output(root);
					break;
				case 2:
					in_order_output(root);
					break;
				default:
					post_order_output(root);
					break;
				}
			}

			default:
				tree_out(root, 0);
				break;
			}
			cout << endl;
		
		
	}
}


